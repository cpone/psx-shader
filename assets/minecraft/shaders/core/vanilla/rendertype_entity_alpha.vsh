#version 150

in vec3 Position;
in vec4 Color;
in vec2 UV0;
in vec2 UV1;
in vec2 UV2;
in vec3 Normal;

uniform mat4 ModelViewMat;
uniform mat4 ProjMat;

out vec4 vertexColor;
out vec2 texCoord0;
out vec2 texCoord1;
out vec2 texCoord2;
out vec4 normal;

void main() {
    gl_Position = ProjMat * ModelViewMat * vec4(Position, 1.0);

    vertexColor = Color;

    gl_Position = vertex_snap(gl_Position);
    vec3 affine0 = calculate_affine(UV0, gl_Position);
    vec3 affine1 = calculate_affine(UV1, gl_Position);
    vec3 affine2 = calculate_affine(UV2, gl_Position);

    texCoord0 = affine0.xy/affine0.z;
    texCoord1 = affine1.xy/affine1.z;
    texCoord2 = affine2.xy/affine2.z;

    normal = ProjMat * ModelViewMat * vec4(Normal, 0.0);
}
