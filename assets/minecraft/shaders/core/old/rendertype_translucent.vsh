#version 150

#moj_import <psx.glsl>

#moj_import <light.glsl>
#moj_import <fog.glsl>

in vec3 Position;
in vec4 Color;
in vec2 UV0;
in ivec2 UV2;
in vec3 Normal;

uniform sampler2D Sampler2;

uniform mat4 ModelViewMat;
uniform mat4 ProjMat;
uniform vec3 ChunkOffset;

out float vertexDistance;
out vec4 vertexColor;
out vec2 texCoord0;
out vec4 normal;

void main() {
    vec3 pos = Position + ChunkOffset;
    gl_Position = ProjMat * ModelViewMat * vec4(pos, 1.0);

    vertexDistance = cylindrical_distance(ModelViewMat, pos);
    vertexColor = Color * minecraft_sample_lightmap(Sampler2, UV2);
    normal = ProjMat * ModelViewMat * vec4(Normal, 0.0);

    gl_Position = vertex_snap(gl_Position);
    vec3 affine = calculate_affine(UV0, gl_Position);

    texCoord0 = affine.xy/affine.z;
}
