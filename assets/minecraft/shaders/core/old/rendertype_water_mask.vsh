#version 150

#moj_import <psx.glsl>

in vec3 Position;

uniform mat4 ModelViewMat;
uniform mat4 ProjMat;

void main() {
    gl_Position = ProjMat * ModelViewMat * vec4(Position, 1.0);

    gl_Position = vertex_snap(gl_Position);
}
