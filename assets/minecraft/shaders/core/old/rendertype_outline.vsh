#version 150

#moj_import <psx.glsl>

in vec3 Position;
in vec4 Color;
in vec2 UV0;

uniform mat4 ModelViewMat;
uniform mat4 ProjMat;

out vec4 vertexColor;
out vec2 texCoord0;

void main() {
    gl_Position = ProjMat * ModelViewMat * vec4(Position, 1.0);

    vertexColor = Color;

    gl_Position = vertex_snap(gl_Position);
    vec3 affine = calculate_affine(UV0, gl_Position);

    texCoord0 = affine.xy/affine.z;
}
