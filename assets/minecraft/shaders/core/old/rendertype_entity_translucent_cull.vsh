#version 150

#moj_import <psx.glsl>

#moj_import <light.glsl>
#moj_import <fog.glsl>

in vec3 Position;
in vec4 Color;
in vec2 UV0;
in vec2 UV1;
in ivec2 UV2;
in vec3 Normal;

uniform sampler2D Sampler2;

uniform mat4 ModelViewMat;
uniform mat4 ProjMat;
uniform mat3 IViewRotMat;

uniform vec3 Light0_Direction;
uniform vec3 Light1_Direction;

out float vertexDistance;
out vec4 vertexColor;
out vec2 texCoord0;
out vec2 texCoord1;
out vec2 texCoord2;
out vec4 normal;

void main() {
    gl_Position = ProjMat * ModelViewMat * vec4(Position, 1.0);

    vertexDistance = cylindrical_distance(ModelViewMat, IViewRotMat * Position);
    vertexColor = minecraft_mix_light(Light0_Direction, Light1_Direction, Normal, Color) * texelFetch(Sampler2, UV2 / 16, 0);

    gl_Position = vertex_snap(gl_Position);
    vec3 affine0 = calculate_affine(UV0, gl_Position);
    vec3 affine1 = calculate_affine(UV1, gl_Position);
    vec3 affine2 = calculate_affine(UV2, gl_Position);

    texCoord0 = affine0.xy/affine0.z;
    texCoord1 = affine1.xy/affine1.z;
    texCoord2 = affine2.xy/affine2.z;

    normal = ProjMat * ModelViewMat * vec4(Normal, 0.0);
}
