#version 150

#moj_import <vsh_util.glsl>
#moj_import <psx.glsl>

in vec3 Position;
in vec2 UV0;
in vec4 Color;

uniform mat4 ModelViewMat;
uniform mat4 ProjMat;

out vec2 texCoord0;
out vec4 vertexColor;

out vec3 affine0;

void main() {
    gl_Position = ProjMat * ModelViewMat * vec4(Position, 1.0);

    texCoord0 = UV0;
    vertexColor = Color;

    if ( !isGUI(ProjMat) ) {
        gl_Position = vertex_snap(gl_Position);
        affine0 = calculate_affine(UV0, gl_Position);
    }
}
