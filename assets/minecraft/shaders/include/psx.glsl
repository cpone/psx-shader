#version 150

vec4 vertex_snap(vec4 vertex) {
    vec2 resolution = vec2(160.0, 120.0);

    vec4 snappedPos = vertex;
    snappedPos.xyz = vertex.xyz / vertex.w; // convert to normalised device coordinates (NDC)
    snappedPos.xy = floor(resolution * snappedPos.xy) / resolution; // snap the vertex to the lower-resolution grid
    snappedPos.xyz *= vertex.w; // convert back to projection-space
    return snappedPos;
}

vec3 calculate_affine(vec2 UV0, vec4 gl_Position) {
    float wVal = max(gl_Position.z, 0.8);
    return vec3(UV0.st * wVal, wVal);
}